package ru.t1.godyna.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.model.Session;

import java.util.List;

public interface ISessionRepository {

    void add(@NotNull final Session session);

    void update(@NotNull final Session session);

    void remove(@NotNull final Session session);

    void removeByUserId(@NotNull final String userId);

    @Nullable
    List<Session> findAll();

    @Nullable
    List<Session> findAll(@NotNull String userId);

    @Nullable
    Session findOneById(@NotNull final String id);

    @Nullable
    Session findOneByIdUserId(@NotNull final String userId, @NotNull final String id);

}
