package ru.t1.godyna.tm.api.repository.model;

import ru.t1.godyna.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
}
