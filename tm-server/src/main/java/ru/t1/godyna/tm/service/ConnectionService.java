package ru.t1.godyna.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.api.service.IConnectionService;
import ru.t1.godyna.tm.api.service.IDatabaseProperty;
import ru.t1.godyna.tm.dto.model.ProjectDTO;
import ru.t1.godyna.tm.dto.model.SessionDTO;
import ru.t1.godyna.tm.dto.model.TaskDTO;
import ru.t1.godyna.tm.dto.model.UserDTO;
import ru.t1.godyna.tm.model.Project;
import ru.t1.godyna.tm.model.Session;
import ru.t1.godyna.tm.model.Task;
import ru.t1.godyna.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperties;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull IDatabaseProperty databaseProperties) {
        this.databaseProperties = databaseProperties;
        this.entityManagerFactory = factory();
    }

    @NotNull
    @Override
    public EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, databaseProperties.getDatabaseDriver());
        settings.put(Environment.URL, databaseProperties.getDatabaseURL());
        settings.put(Environment.USER, databaseProperties.getDatabaseUsername());
        settings.put(Environment.PASS, databaseProperties.getDatabasePassword());
        settings.put(Environment.DIALECT, databaseProperties.getDatabaseDialect());
        settings.put(Environment.HBM2DDL_AUTO, databaseProperties.getDatabaseHbm2DdlAuto());
        settings.put(Environment.SHOW_SQL, databaseProperties.getDatabaseShowSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperties.getDatabaseSecondLevelCache());
        settings.put(Environment.CACHE_REGION_FACTORY, databaseProperties.getDatabaseFactoryClass());
        settings.put(Environment.USE_QUERY_CACHE, databaseProperties.getDatabaseUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, databaseProperties.getDatabaseUseMinPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, databaseProperties.getDatabaseRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperties.getDatabaseConfigFilePath());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}
