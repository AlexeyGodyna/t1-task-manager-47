package ru.t1.godyna.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = "login", nullable = false, length = 50)
    private  String login;

    @NotNull
    @Column(name = "password_hash", nullable = false, length = 200)
    private String passwordHash;

    @Nullable
    @Column(name = "email", nullable = true)
    private String email;

    @Nullable
    @Column(name = "fst_name", nullable = true, length = 50)
    private String firstName;

    @Nullable
    @Column(name = "lst_name", nullable = true, length = 50)
    private String lastName;

    @Nullable
    @Column(name = "mdl_name", nullable = true, length = 50)
    private String middleName;

    @NotNull
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(name = "lock_flg", nullable = false)
    private Boolean locked = false;

}
