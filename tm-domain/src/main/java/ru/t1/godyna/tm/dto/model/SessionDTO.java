package ru.t1.godyna.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import ru.t1.godyna.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class SessionDTO extends AbstractUserOwnedModelDTO {

    @NotNull
    @Column(name = "created", nullable = false)
    private Date date = new Date();

    @Nullable
    @Enumerated(EnumType.STRING)
    @Column(name = "role", nullable = true)
    private Role role = null;

}
